//
//  SchoolDetailViewController.swift
//  20220127-MuthuPragalathan-NYCSchools
//
//  Created by Ajay on 1/29/22.
//

import UIKit

class SchoolDetailViewController: UIViewController {

    lazy var schoolListView: SchoolListView = {
        let result = SchoolListView { [weak self] (options, object) in
            switch options {
            case .schoolSelected:
                print("School selected - \(object?.schoolName ?? "" )")
            }
        }
        return result
    }()
    
    var selectedSchool: SchoolDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        // Do any additional setup after loading the view.
    }

    func setUpView(){
        schoolListView.shouldDisplayDetailsInCell = true
        schoolListView.selectedSchool = selectedSchool
        view.addSubview(schoolListView)
        schoolListView.translatesAutoresizingMaskIntoConstraints = false
        schoolListView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        schoolListView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        schoolListView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20).isActive = true
        schoolListView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -20).isActive = true
        
    }

}
