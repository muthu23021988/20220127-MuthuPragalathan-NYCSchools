//
//  LandingViewController.swift
//  20220127-MuthuPragalathan-NYCSchools
//
//  Created by Ajay on 1/29/22.
//

import UIKit

class LandingViewController: UIViewController {

    var interactor: LandingInteractorProtocol?
    var selectedSchool: SchoolDetails?
    lazy var schoolListView: SchoolListView = {
        let result = SchoolListView { [weak self] (options, object) in
            switch options {
                // School selection call back.
            case .schoolSelected:
                let uistoryBoard = UIStoryboard(name: "Main", bundle: nil)
                
                let detailViewController = uistoryBoard.instantiateViewController(withIdentifier: "SchoolDetailViewController") as! SchoolDetailViewController
                self?.selectedSchool = object
                detailViewController.selectedSchool = self?.selectedSchool
                
                self?.navigationController?.pushViewController(detailViewController, animated: true)
            }
        }
        return result
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        setUpClearArchStack()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor?.fetchSchoolList()
    }
    
    func setUpView(){
        // Add table view as a child view.
        view.addSubview(schoolListView)
        schoolListView.translatesAutoresizingMaskIntoConstraints = false
        schoolListView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        schoolListView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        schoolListView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20).isActive = true
        schoolListView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -20).isActive = true
        
    }
    
    // Clean swift architecture set up
    private func setUpClearArchStack() {
        let viewInteractor = LandingViewInteractor()
        let presenter = LandingViewPresenter()
        interactor = viewInteractor
        viewInteractor.presenter = presenter
        presenter.viewController = self
    }
    
}

extension LandingViewController: LandingViewProtocol {
    func fetchSchoolListSuccess(schoolList: [SchoolDetails]) {
        // Success call back from presenter.
        schoolListView.schoolDetailList = schoolList
    }
    
    func fetchSchoolListFailed(errorDetail: String) {
        // Failure call back from presenter.
        print(errorDetail)
    }
    
    
}
