//
//  LandingViewPresenter.swift
//  20220127-MuthuPragalathan-NYCSchools
//
//  Created by Ajay on 1/29/22.
//

import Foundation

class LandingViewPresenter: LandingPresenterProtocol {
    var viewController: LandingViewProtocol?
    func fetchSchoolListReponseSuccess(schoolList: [SchoolDetails]) {
        viewController?.fetchSchoolListSuccess(schoolList: schoolList)
    }
    
    func fetchSchoolListFailure(errorDetail: String) {
        viewController?.fetchSchoolListFailed(errorDetail: "Failed")
    }
    
    
}
