//
//  LandingViewInteractor.swift
//  20220127-MuthuPragalathan-NYCSchools
//
//  Created by Ajay on 1/29/22.
//

import Foundation

class LandingViewInteractor: LandingInteractorProtocol {
    var presenter: LandingPresenterProtocol?
    func fetchSchoolList() {
        
        /*
         // MARK: - Fetch school list from API.
        var request = URLRequest(url: URL(string: "https://data.cityofnewyork.us/Education/DOE-High-School-Directory-2017/s3k6-pzi2")!)
        request.httpMethod = "GET"
        request.httpBody = nil
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            print(response!)
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                print(json)
            } catch {
                print("error")
            }
        })
         task.resume()
         // - API is not working. Response is in the HTML format.
         */
        
        // MARK: - Fetch and parse the school list from the local jSon file.
        
        let schoolListResponse = fetchSchoolListFromLocalResouce()
        guard let schoolList = schoolListResponse else {
            // Get school list call Failed.
            self.presenter?.fetchSchoolListFailure(errorDetail: "Failed")
            return
        }
        
        let list = getSchoolModelsListFromResponse(schoolList: schoolList)
        // Data parsed successfully.
        self.presenter?.fetchSchoolListReponseSuccess(schoolList: list)
    }
    
    func fetchSchoolListFromLocalResouce() -> [[Any]]? {
        // MARK: - Fetch and parse the school list from the local jSon file.
        if let path = Bundle.main.path(forResource: "NYC-school", ofType: "json") {
            do {
                  let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                  let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                  if let jsonResult = jsonResult as? Dictionary<String, AnyObject> {
                            // do stuff
                      if let data = jsonResult["data"] {
                          return data as? [[Any]]
                      } else {
                          return nil
                      }
                  } else {
                      return nil
                  }
              } catch {
                  return nil
              }
        } else {
            return nil
        }
    }
    
    // Create data model.
    func getSchoolModelsListFromResponse(schoolList: [[Any]]) -> [SchoolDetails] {
        var schoolModelList: [SchoolDetails] = []
       
        for listIndex in 0..<schoolList.count - 1 {
            var schoolModel = SchoolDetails()
            let schoolDetails = schoolList[listIndex]
                    
            // Check and unrap the values.
            if schoolDetails.indices.contains(8) {
                if let dbn = schoolDetails[8] as? String {
                    schoolModel.dBN = dbn
                }
            }
            
            if schoolDetails.indices.contains(9) {
                if let schoolName = schoolDetails[9] as? String {
                    schoolModel.schoolName = schoolName
                }
            }
            
            if schoolDetails.indices.contains(10) {
                if let testTakers = schoolDetails[10] as? String {
                    schoolModel.numberOfSATTestTakers = testTakers
                }
            }
            
            if schoolDetails.indices.contains(11) {
                if let readingScore = schoolDetails[11] as? String {
                    schoolModel.readingScore = readingScore
                }
            }
            if schoolDetails.indices.contains(12) {
                if let mathScore = schoolDetails[12] as? String {
                    schoolModel.mathScore = mathScore
                }
            }
            if schoolDetails.indices.contains(13) {
                if let writingScore = schoolDetails[13] as? String {
                    schoolModel.writingScore = writingScore
                }
            }
            
            schoolModelList.append(schoolModel)
            
        }
        return schoolModelList
        }
        
        
        
    
    
}
