//
//  SchoolDetailTableViewCell.swift
//  20220127-MuthuPragalathan-NYCSchools
//
//  Created by Ajay on 1/29/22.
//

import Foundation
import UIKit

class SchoolDetailTableViewCell: UITableViewCell {
    // MARK: - Declarations
    var schoolDetail: SchoolDetails?
    lazy var dBNLabel: UILabel = {
       let result = UILabel()
        result.text = "DBN: \(schoolDetail?.dBN ?? "")"
        result.font = UIFont.preferredFont(forTextStyle: .body)
        result.textColor = .gray
        return result
    }()
    
    lazy var schoolNameLabel: UILabel = {
       let result = UILabel()
        result.text = "School Name: \(schoolDetail?.schoolName ?? "")"
        result.font = UIFont.preferredFont(forTextStyle: .title2)
        result.numberOfLines = 0
        result.textColor = .black
        return result
    }()
    
    lazy var numberOfTestTakersLabel: UILabel = {
       let result = UILabel()
        result.text = "Num of SAT Test Takers: \(schoolDetail?.numberOfSATTestTakers ?? "")"
        result.font = UIFont.preferredFont(forTextStyle: .body)
        result.textColor = .gray
        return result
    }()
    
    lazy var readingScoreLabel: UILabel = {
       let result = UILabel()
        result.text = "Avg. Reading Score: \(schoolDetail?.readingScore ?? "")"
        result.font = UIFont.preferredFont(forTextStyle: .body)
        result.textColor = .gray
        return result
    }()
    
    lazy var mathScoreLabel: UILabel = {
       let result = UILabel()
        result.text = "Avg. Math Score: \(schoolDetail?.mathScore ?? "")"
        result.font = UIFont.preferredFont(forTextStyle: .body)
        result.textColor = .gray
        return result
    }()
    
    lazy var writingScoreLabel: UILabel = {
       let result = UILabel()
        result.text = "Avg. Writing Score: \(schoolDetail?.writingScore ?? "")"
        result.font = UIFont.preferredFont(forTextStyle: .body)
        result.textColor = .gray
        return result
    }()
    
    // Parent stack view
    lazy var stackView: UIStackView = {
     let result = UIStackView(arrangedSubviews: [schoolNameLabel, dBNLabel, numberOfTestTakersLabel, readingScoreLabel, mathScoreLabel, writingScoreLabel])
        result.alignment = .fill
        result.axis = .vertical
        result.spacing = 15
        result.distribution = .fill
        result.contentMode = .scaleAspectFit
        return result
    }()
    
    // MARK: - Custom Methods
    func setUpCell(){
        contentView.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20).isActive = true
        stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20).isActive = true
        stackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20).isActive = true
        stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20).isActive = true
    }
}


