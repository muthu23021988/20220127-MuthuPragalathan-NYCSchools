//
//  SchoolListView.swift
//  20220127-MuthuPragalathan-NYCSchools
//
//  Created by Ajay on 1/29/22.
//

import UIKit


class SchoolListView: UITableView {
    
    enum ListSelectionOption{
        case schoolSelected
    }
    
    var completion: (ListSelectionOption, SchoolDetails?) -> Void
    
    init(completion: @escaping(ListSelectionOption, SchoolDetails?) -> Void) {
        self.completion =  completion
        super.init(frame: .zero, style: .plain)
        setupTableView()
    }
    
    var selectedSchool: SchoolDetails?
    
    var shouldDisplayDetailsInCell = false
    
    required init?(coder: NSCoder) {
        return nil
    }
    
    var schoolDetailList: [SchoolDetails]?{
        didSet {
            // Refresh the list once the response is received.
            reloadData()
        }
    }
    
    func setupTableView(){
        register(SchoolDetailTableViewCell.self, forCellReuseIdentifier: "SchoolDetailTableViewCell")
        delegate = self
        dataSource = self
        backgroundColor = .white
        showsVerticalScrollIndicator = true
    }
}

// MARK: - Table View Delegat and Data Source Methods
extension SchoolListView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        (shouldDisplayDetailsInCell) ? 1 : schoolDetailList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Display Default cell for school list.
        if !shouldDisplayDetailsInCell {
       var tableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        if tableViewCell == nil {
            tableViewCell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        }
        
        guard let cell = tableViewCell else {
            return UITableViewCell()
        }
        if let school = schoolDetailList?[indexPath.row]{
            cell.textLabel?.text = school.schoolName
        } else {
            cell.textLabel?.text = ""
        }
        cell.textLabel?.numberOfLines = 0
        return cell
        } else {
            // Display custom cell for details.
            if let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolDetailTableViewCell", for: indexPath) as? SchoolDetailTableViewCell {
                cell.selectionStyle = .none
                cell.schoolDetail = selectedSchool
                cell.setUpCell()
                return cell
            } else {
                return UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // School selection call back. 
        self.completion(.schoolSelected, schoolDetailList?[indexPath.row] ?? nil)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        // Table Header
        (shouldDisplayDetailsInCell) ? "NYC School Details" : "NYC High Schools"
    }
}

