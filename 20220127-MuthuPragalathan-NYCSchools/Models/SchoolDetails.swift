//
//  SchoolDetails.swift
//  20220127-MuthuPragalathan-NYCSchools
//
//  Created by Ajay on 1/29/22.
//

import Foundation
// School Data Model 
struct SchoolDetails{
    var dBN: String?
    var schoolName: String?
    var numberOfSATTestTakers: String?
    var readingScore: String?
    var mathScore: String?
    var writingScore: String?
    
}
