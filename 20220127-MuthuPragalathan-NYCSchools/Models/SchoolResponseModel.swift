//
//  SchoolResponseModel.swift
//  20220127-MuthuPragalathan-NYCSchools
//
//  Created by Muthu on 1/27/22.
//

import Foundation


struct SchoolResponseModel: Decodable {
    var data: [[Any]]?
        
    public init<T>(_ value :[[T]]?) {
        self.data = value ?? [[]]
      }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let string = try? container.decode([[String]].self) {
            self.init([[string]])
        } else if let int = try? container.decode([[Int]].self){
            self.init([[int]])
        } else {
            self.init([[]])
        }
    }
}
    
