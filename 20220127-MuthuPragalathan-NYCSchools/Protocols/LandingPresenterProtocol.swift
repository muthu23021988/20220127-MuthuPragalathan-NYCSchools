//
//  LandingPresenterProtocol.swift
//  20220127-MuthuPragalathan-NYCSchools
//
//  Created by Ajay on 1/29/22.
//

import Foundation

protocol LandingPresenterProtocol{
    func fetchSchoolListReponseSuccess(schoolList: [SchoolDetails])
    func fetchSchoolListFailure(errorDetail: String)
}

