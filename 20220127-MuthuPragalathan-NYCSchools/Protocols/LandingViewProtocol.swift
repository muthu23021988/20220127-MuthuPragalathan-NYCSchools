//
//  LandingViewProtocol.swift
//  20220127-MuthuPragalathan-NYCSchools
//
//  Created by Ajay on 1/29/22.
//

import Foundation

protocol LandingViewProtocol{
    func fetchSchoolListSuccess(schoolList: [SchoolDetails])
    func fetchSchoolListFailed(errorDetail: String)
}
