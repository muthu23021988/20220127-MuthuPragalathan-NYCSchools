//
//  SchoolDetailViewTest.swift
//  20220127-MuthuPragalathan-NYCSchoolsTests
//
//  Created by Ajay on 1/30/22.
//  Copyright © 2022 Muthu. All rights reserved.
//

import Foundation
import XCTest
import UIKit
@testable import _0220127_MuthuPragalathan_NYCSchools

class SchoolDetailViewTest: XCTestCase{
    
    
    func testViewUISetUp() {
//        let detailViewController = SchoolDetailViewController()
        let uistoryBoard = UIStoryboard(name: "Main", bundle: nil)
        
        let detailViewController = uistoryBoard.instantiateViewController(withIdentifier: "SchoolDetailViewController") as! SchoolDetailViewController
        
        let selectedSchool = SchoolDetails(dBN: "123", schoolName: "Test", numberOfSATTestTakers: "199", readingScore: "100", mathScore: "100", writingScore: "100")

        detailViewController.selectedSchool = selectedSchool
        detailViewController.viewDidLoad()
        
        let listView = detailViewController.schoolListView
        detailViewController.schoolListView.shouldDisplayDetailsInCell = true
        detailViewController.schoolListView.selectedSchool = selectedSchool
        detailViewController.schoolListView.reloadData()
        XCTAssertNotNil(listView)
    }
    
}
