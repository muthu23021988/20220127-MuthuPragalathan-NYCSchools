//
//  LandingViewTest.swift
//  20220127-MuthuPragalathan-NYCSchoolsTests
//
//  Created by Ajay on 1/30/22.
//  Copyright © 2022 Muthu. All rights reserved.
//

import Foundation

import UIKit

@testable import _0220127_MuthuPragalathan_NYCSchools
import XCTest

class LandingViewTest: XCTestCase {
    
    var schoolList: [[Any]]?
    var schoolListModel: [SchoolDetails]?
    func testSchoolListResponse() {
        let interactor = LandingViewInteractor()
        schoolList = interactor.fetchSchoolListFromLocalResouce()
        XCTAssertNotNil(schoolList)
    }
    
    func testSchoolListModel() {
        let interactor = LandingViewInteractor()
        let modelList = interactor.getSchoolModelsListFromResponse(schoolList: schoolList ?? [[]])
        schoolListModel = modelList
        XCTAssertNotNil(modelList)
    }
    
    func testLandingViewUI() {
        let viewController = LandingViewController()
        let listView = viewController.schoolListView
        
        XCTAssertNotNil(listView)
        
        viewController.schoolListView.schoolDetailList = schoolListModel
        
        let indexPath = IndexPath(row: 0, section: 0)
        viewController.schoolListView.tableView(viewController.schoolListView, didSelectRowAt: indexPath)
//        XCTAssertNotNil(viewController.selectedSchool)
}

}




